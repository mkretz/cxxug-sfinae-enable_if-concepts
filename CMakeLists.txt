cmake_minimum_required(VERSION 3.10)

project(demo)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

set(CMAKE_CXX_STANDARD 23)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

add_executable(sfinae sfinae.c++)
add_executable(enable_if enable_if.c++)
add_executable(concepts concepts.c++)
