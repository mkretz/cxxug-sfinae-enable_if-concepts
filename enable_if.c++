#include <format>
#include <iostream>
#include <type_traits>
#include <cassert>

template <typename T>
class bounded_value
{
  protected:
    T value;

    bounded_value() = default;

    bounded_value(T x)
        : value(x)
    {
    }
};

template <typename T, T lo, T hi>
class bounded
    : public std::conditional_t<lo == hi, std::integral_constant<T, lo>, bounded_value<T>>
{
  using base = std::conditional_t<lo == hi, std::integral_constant<T, lo>, bounded_value<T>>;

  using base::value;

public:
  template <T l = lo, T h = hi,
           typename = std::enable_if_t<(l == h or (l <= T() and h >= T()))>>
  bounded()
      : base()
  {
  }

  template <auto l = lo, auto h = hi,
            typename = std::enable_if_t<(l < h)>>
  bounded(T new_value)
      : base{new_value}
  {
    assert(lo <= new_value and new_value <= hi);
  }

  static constexpr std::integral_constant<T, lo> lower_bound = {};

  static constexpr std::integral_constant<T, hi> upper_bound = {};

  explicit operator T() const
  {
    return value;
  }

};

static_assert(std::is_default_constructible_v<bounded<int, 0, 0>>);
static_assert(std::is_default_constructible_v<bounded<int, 0, 5>>);
static_assert(not std::is_default_constructible_v<bounded<int, 1, 5>>);
static_assert(std::is_default_constructible_v<bounded<int, 5, 5>>);

int main()
{
  bounded<int, 0, 5> a;
  bounded<int, 4, 4> b;
  //auto c = a + b;
  std::cout << std::format("sizeof({}) == {}, sizeof({}) = {}, {}\n",
      int(a), sizeof(a), int(b), sizeof(b), int(a));
}
