#include <concepts>
#include <vector>
#include <ranges>


template <std::ranges::sized_range T>
std::ranges::range_value_t<T>
sum(T&& v)
{
  using value_type = std::ranges::range_value_t<T>;
  value_type acc = v[0];
  for (unsigned i = 1; i < std::ranges::size(v); ++i) {
    acc += v[i];
  }
  return acc;
}


int main()
{
  int a[4] = {1, 2, 3, 4};
  sum(a);
  return 0;
}
