#include <array>
#include <format>
#include <iostream>
#include <vector>

template <typename T>
typename T::value_type
sum(const T& v)
{
  typename T::value_type acc = v[0];
  for (unsigned i = 1; i < v.size(); ++i) {
    acc += v[i];
  }
  return acc;
}

template <typename T>
std::enable_if_t<std::is_arithmetic_v<T>, T>
sum(T x)
{
  return x;
}

int main()
{
  std::vector a = {1., 2., 3., 4.};
  std::array b = {3, 4, 5};
  int c = 6;
  std::cout << std::format("{} {} {}\n", sum(a), sum(b), sum(c));
  return 0;
}
